FROM python:3.8-alpine

ENV FLASK_VERSION=2.0.1

RUN apk --no-cache --virtual .build add build-base libffi-dev && \
    pip install flask==${FLASK_VERSION} gunicorn gevent && \
    apk del .build

WORKDIR /
ADD *.py /
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER 1000

ENTRYPOINT ["/entrypoint.sh"]
