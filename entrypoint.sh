#!/usr/bin/env sh

if [ -n "$GUNICORN_CONFIG" ] ; then
  exec gunicorn -c $GUNICORN_CONFIG --access-logfile - kubeless:app $@
else
  exec gunicorn --access-logfile - kubeless:app $@
fi
