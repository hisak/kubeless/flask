import os
import math

workers = int(os.getenv('GUNICORN_WORKERS', math.ceil(os.cpu_count() / 2)))
worker_class = os.getenv('GUNICORN_WORKER_CLASS', 'gevent')
logger_class = 'log.Logger'
access_log_format = '%(h)s %(l)s %(u)s %(t)s “%(r)s” %(s)s %(b)s “%(f)s” “%(a)s” %(T)s'
bind = '0.0.0.0:{}'.format(os.getenv('FUNC_PORT', 8080))
timeout = os.getenv('FUNC_TIMEOUT', 180)
