#!/usr/bin/env python

import os
from importlib.machinery import SourceFileLoader
from flask import Flask

app = Flask(__name__)
app.register_blueprint(
    getattr(SourceFileLoader('function',
                             '{}/{}.py'.format(os.getenv('KUBELESS_INSTALL_VOLUME'),
                                               os.getenv('MOD_NAME'))).load_module(),
            os.getenv('FUNC_HANDLER')))


@app.route('/healthz')
def healthz():
    return 'OK'
