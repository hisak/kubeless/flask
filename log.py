import logging
import gunicorn.glogging


class LoggerDropFilter(logging.Filter):
    def filter(self, record):
        return " /healthz " not in record.getMessage()


class Logger(gunicorn.glogging.Logger):
    def setup(self, cfg):
        super().setup(cfg)
        logger = logging.getLogger("gunicorn.access")
        logger.addFilter(LoggerDropFilter())

    def atoms(self, resp, req, environ, request_time):
        atoms = super().atoms(resp, req, environ, request_time)
        atoms['T'] = '{}/{}'.format(request_time.seconds, request_time.microseconds)
        if 'HTTP_X_FORWARDED_FOR' in environ:
            atoms['h'] = environ.get('HTTP_X_FORWARDED_FOR')
        return atoms
